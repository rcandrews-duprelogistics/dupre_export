﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DupreExport
{
    class HelperClass
    {

    }
    public class Commodity
    {
        public int TankID { get; set; }
        public decimal Volume { get; set; }
        public string TankTranslation { get; set; }
        public string CommodityCode { get; set; }
        //public List<DeliveryTank> DeliveryTanks { get; set; }
        //public decimal GrossGallons { get; set; }
        public string PinCode { get; set; }
        public bool Existing { get; set; }
        public string BOL { get; set; }
        public string Supplier { get; set; }
    }
    public class DeliveryTank
    {
        public int TankID { get; set; }
        public decimal Volume { get; set; }
        public string TankTranslation { get; set; }
    }
}
