﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace DupreExport
{
    public class MxPhersonService
    {
        private TMWService.APIWCFServicesClient _tmwWebService = null;
        private McpService.exportVMISOFromDupreToJDEBPELClient _mcpWebService = null;
        private McpBolService.exportBOLfromDupreBizSpeedtoJDEClient _mcpBolService = null;

        private string mappingType = "MCPHER";

        #region Export All
        public string ExportAll(ExportCriteria ec)
        {
            try
            {
                System.Net.ServicePointManager.Expect100Continue = false;

                using (var context = new TMW_DataEntities())
                {
                    /*
                     *  To be uncommented and test after stored procedure logic is added
                     */

                    //var ordersList = context.GetDupreServiceQueue("UPRE2K\\DLTMWDAWGDEV$", 3, "1.1").ToList<DupreServiceQueue>();
                    ObjectResult<string> ordersList = context.GetDupreServiceQueue("UPRE2K\\DLTMWDAWGDEV$", 3, "1.1");
                    //foreach (DupreServiceQueue o in ordersList)
                    //{
                    //    try
                    //    {
                    //        int orderNumber = Convert.ToInt32(o.local_order_number);
                    //        ExportOrderAndBol(orderNumber, ec);

                    //    }
                    //    catch (Exception)
                    //    {
                    //        //Message was logged in ExportOrder method, so we can continue to process rest of the orders
                    //    }
                    //}
                    foreach (string o in ordersList)
                    {
                        try
                        {
                            int orderNumber = Convert.ToInt32(o);
                            ExportOrderAndBol(orderNumber, ec);
                        }
                        catch (Exception)
                        {
                            //Message was logged in ExportOrder method, so we can continue to process rest of the orders
                        }

                    }
                }
            return "Finished Export";
            }
            catch (Exception ex)
            {
                LogMessage("Export All", "Exception", ex.Message, "", "", false);
                //throw;
                return ex.Message;
            }
            finally
            {
                CloseMCPService();
                CloseMCPBOLService();
                CloseTMWService();
            }
        }
        #endregion

        #region Export Single Order
        public string ExportSingleOrder(ExportCriteria ec)
        {
            try
            {
                System.Net.ServicePointManager.Expect100Continue = false;
                ExportOrderAndBol(ec.OrderNumber, ec);
                
                return $"Exported order {ec.OrderNumber}";

            }
            catch (Exception ex)
            {
                LogMessage("Export Single Order", "Exception", ex.Message, ec.OrderNumber.ToString(), "", false);
                return ex.Message;
                //throw;
            }
            finally
            {
                CloseMCPService();
                CloseMCPBOLService();
                CloseTMWService();
            }
        }
        #endregion

        #region Helper Methods
        private void ExportOrderAndBol(int orderNumber, ExportCriteria ec)
        {
            TMWService.Order fuelOrder = GetTMWOrder(orderNumber);
            string mcpOrderNum = null;

           List<TMWService.Stop> stList = fuelOrder.Stops.Where(s => s.StopType == "DRP").ToList();

            CheckAndCreateRefNumbers(fuelOrder, stList);

            foreach (TMWService.Stop st in stList)
            {
                mcpOrderNum = st.ReferenceNumbers.Where(x => x.ReferenceType == mappingType).Select(x => x.Value).FirstOrDefault();
                List<Commodity> cmdList = GetRemoteCommodities(st.Freights);
                string consignee = GetRemoteCode("CON", mappingType, st.CompanyID);
                if (ec.ExportOrder)
                {
                ExportOrder(fuelOrder, cmdList, mcpOrderNum, st, consignee, false);
                }
                if (fuelOrder.Status == "CMP" && ec.ExportBOL)
                {
                    ExportBOL(fuelOrder, cmdList, mcpOrderNum, st, consignee);
                }
                
            }

        }
        private void CheckAndCreateRefNumbers(TMWService.Order fuelOrder, List<TMWService.Stop> stList)
        {
            string mcpOrderNum = null;
            if (stList.Count > 1)
            {
                //consolidated order so check the ref for both stops
                foreach (TMWService.Stop st in stList)
                {
                    mcpOrderNum = st.ReferenceNumbers.Where(x => x.ReferenceType == mappingType).Select(x => x.Value).FirstOrDefault();
                    if (string.IsNullOrEmpty(mcpOrderNum))
                    {
                        if (fuelOrder.Status == "CMP")
                        {
                            //will export with "N" flag to create order in McPherson system
                            List<Commodity> cmdList = GetRemoteCommodities(st.Freights);
                            string consignee = GetRemoteCode("CON", mappingType, st.CompanyID);
                            ExportOrder(fuelOrder, cmdList, mcpOrderNum, st, consignee, true);
                        }
                        else
                        {
                            throw new Exception($"Consolidated order does not have MCP ref number for all the stops and is in {fuelOrder.Status} status");
                        }
                    }
                }
            }
            else
            {
                TMWService.Stop st = fuelOrder.Stops.Where(s => s.StopType == "DRP").FirstOrDefault();
                mcpOrderNum = st.ReferenceNumbers.Where(x => x.ReferenceType == mappingType).Select(x => x.Value).FirstOrDefault();
                if (string.IsNullOrEmpty(mcpOrderNum) && fuelOrder.Status != "AVL")
                {
                    throw new Exception($"Order without MCP ref number and has {fuelOrder.Status} status");
                }
            }

        }
        private void ExportOrder(TMWService.Order fuelOrder, List<Commodity> cmdList, string mcpOrderNum, TMWService.Stop st, string consignee, bool isIdentifierN)
        {
            List<McpService.SODupre> fOrd = new List<McpService.SODupre>();

            foreach (Commodity c in cmdList)
            {
                McpService.SODupre ord = null;
                //ordCol.VMISODupre = null;
                ord = new McpService.SODupre();
                ord.Customer_TankNumber = c.TankTranslation;
                string ord_Line = c.TankID.ToString();
                ord.JDEOrderLine = ord_Line.PadRight(4, '0');
                if (mcpOrderNum == null || mcpOrderNum == "" || fuelOrder.Status == "AVL")
                {
                    //new order - sending delivery window
                    ord.DeliveryWindowStartDate_Time = st.EarliestDate.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
                    ord.DeliveryWindowEndDate_Time = st.LatestDate.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
                }
                else if (fuelOrder.Status == "CAN")
                {
                    ord.DeliveryWindowStartDate_Time = "";
                    ord.DeliveryWindowEndDate_Time = "";

                }
                else
                {
                    // Send eta
                    ord.DeliveryWindowStartDate_Time = st.ArrivalDate.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
                    ord.DeliveryWindowEndDate_Time = st.DepartureDate.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
                }

                ord.Order_Identifier = fuelOrder.Status;
                
                if (fuelOrder.Status == "DSP" || fuelOrder.Status == "STD")
                {
                    ord.Order_Identifier = "PLN";
                }

                if (isIdentifierN)
                {
                    ord.Order_Identifier = "N";
                }

                ord.JDEOrderNo = mcpOrderNum;
                ord.Dupre_VMI_Order_Number = fuelOrder.ord_hdrnumber.ToString();
                ord.OrderNotes_1 = fuelOrder.Remark;
                ord.JDE_Product = c.CommodityCode;
                ord.JDE_ShipTo = consignee;
                ord.Quantity_Gallons = c.Volume.ToString();
                fOrd.Add(ord);
            }
            LogMessage(fOrd,null, "Export Order", "Info", "Exporting " + fuelOrder.ord_hdrnumber, fuelOrder.ord_hdrnumber.ToString(), "", false);

            //JDE requires to send order in "DSP" status before sending order with "CMP" status
            if (fuelOrder.Status == "CMP" && !isIdentifierN)
            {
                fOrd = fOrd.Select(x => { x.Order_Identifier = "DSP"; return x; }).ToList();
                ExportOrder(fuelOrder, fOrd);
                //JDE requires to send all accesorials with only one product line. So sending it with the first one.
                LoadAcCharges(fuelOrder.ord_hdrnumber,fOrd.FirstOrDefault());
                fOrd = fOrd.Select(x => { x.Order_Identifier = fuelOrder.Status; return x; }).ToList();
            }

            string custSysOrder =  ExportOrder(fuelOrder, fOrd);

            AddCustomerOrderNumber(st, fuelOrder, custSysOrder);
            //UpdateTMWOrder(fuelOrder, custSysOrder);

            if(fuelOrder.OrderSource == "FRCST")
            {
                //need to create primary key in the table before we can call this method to insert
                InsertToDuprePamsXref(fuelOrder.ord_hdrnumber.ToString());
            }

        }


        private string ExportOrder(TMWService.Order fuelOrder, List<McpService.SODupre> fOrd)
        {
            McpService.soOutputVO o = PushOrder(fOrd);

            string custSysOrder = o.outputVO.Select(r => r.documentOrderNoInvoiceetc).FirstOrDefault().ToString();
            LogMessage(fOrd, o, "Export Order", "Info", "Exported " + fuelOrder.ord_hdrnumber, fuelOrder.ord_hdrnumber.ToString(), custSysOrder, false);
            return custSysOrder;
        }
        private McpService.soOutputVO PushOrder(List<McpService.SODupre> co)
        {
            InitializeMCPService();
            McpService.soOutputVO o = _mcpWebService.process(co.ToArray());
            CloseMCPService();
            return o;
        }
        private void ExportBOL(TMWService.Order fuelOrder, List<Commodity> cmdList, string mcpOrderNum, TMWService.Stop st, string consignee)
        {
            List<McpBolService.BOL> fBl = new List<McpBolService.BOL>();

            foreach (Commodity c in cmdList)
            {
                McpBolService.BOL bl = new McpBolService.BOL();
                bl.BOLNumber = c.BOL;
                if (string.IsNullOrEmpty(bl.BOLNumber))
                {
                    continue;
                }
                bl.grossGallons = Convert.ToInt32(c.Volume);
                bl.grossGallonsSpecified = true;
                bl.netGallons = Convert.ToInt32(c.Volume);
                bl.netGallonsSpecified = true;
                bl.productCode = c.CommodityCode;
                bl.JDEOrderNumber = Convert.ToInt32(mcpOrderNum);
                bl.JDEOrderNumberSpecified = true;
                bl.partnerOrderNumber = fuelOrder.ord_hdrnumber.ToString();
                bl.shipTo = Convert.ToInt32(consignee);
                bl.shipToSpecified = true;
                int supp;
                if (int.TryParse(c.Supplier, out supp))
                {
                    bl.supplyPoint = supp;
                    bl.supplyPointSpecified = true;
                }
                bl.transaction_Originator = "DUPRE";

                //McPherson needs delivery date time
                bl.szbolRequestedDateString = st.ArrivalDate.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'"); 
                
                fBl.Add(bl);

            }
            if (fBl.Count > 0)
            {

            McpBolService.BOLJDEOutput[] o = PushBOL(fBl);
                //Not logging output
                LogMessage(fBl, null, "Export BOL", "Info", "Exported BOL for order " + fuelOrder.ord_hdrnumber.ToString(), fuelOrder.ord_hdrnumber.ToString(), "", false);
            }

        }

        private McpBolService.BOLJDEOutput[] PushBOL(List<McpBolService.BOL> co)
        {
            InitializeMCPBOLService();
            McpBolService.BOLJDEOutput[] o = _mcpBolService.process(co.ToArray());
            CloseMCPService();
            return o;
        }
        private void AddCustomerOrderNumber(TMWService.Stop st, TMWService.Order fo, string customerSystemOrder)
        {
            fo.UpdateableProperties = new TMWService.Order.OrderUpdateInfo();
            st.UpdateableProperties = new TMWService.Stop.StopUpdateInfo();

            int count = st.ReferenceNumbers.Where(re => re.Value == customerSystemOrder && re.ReferenceType == mappingType && re.ReferenceTable == "stops").Count();
            if (count > 0)
            {
                return;
            }


            TMWService.ReferenceNumber r = new TMWService.ReferenceNumber();
            r.UpdateableProperties = new TMWService.ReferenceNumber.ReferenceNumberUpdateInfo();

            r.IsNew = true;

            r.ReferenceType = mappingType;
            r.UpdateableProperties.ReferenceType = true;

            r.Value = customerSystemOrder;
            r.UpdateableProperties.Value = true;

            r.ReferenceTable = "stops";
            r.UpdateableProperties.ReferenceTable = true;

            r.ReferenceTableKey = st.StopNumber;
            r.UpdateableProperties.ReferenceTableKey = true;

            r.OrderHeaderNumber = st.ord_hdrnumber;
            r.UpdateableProperties.OrderHeaderNumber = true;

            //refList.Add(r);
            st.ReferenceNumbers.Add(r);
            st.UpdateableProperties.ReferenceNumbers = true;
            fo.UpdateableProperties.Stops = true;
            List<TMWService.Order> ordList = new List<TMWService.Order>();
            ordList.Add(fo);

            InitializeTMWService();
            TMWService.Order.ReturnObject retObj = _tmwWebService.SaveOrder(ordList, true);
            //TMWService.ReferenceNumber.ReturnObject retObj = _tmwWebService.SaveReferenceNumber(refList, true);

            if (retObj.ReturnCode == 0)
            {
                TMWService.Order o = retObj.ReferenceObjects[0];
            }
            else
            {
                string msg = string.Join(";", retObj.ErrorMessages.ToArray());
                throw new Exception(msg);
            }

        }
        private enum Charge { PC, SN };
        private void LoadAcCharges(int ordNumber, McpService.SODupre mo)
        {
            InitializeTMWService();
            TMWService.InvoiceDetail.Criteria crit = new TMWService.InvoiceDetail.Criteria();
            crit.ord_hdrnumber = ordNumber;
            TMWService.InvoiceDetail.ReturnObject retObj = _tmwWebService.RetrieveInvoiceDetail(crit);
            if (retObj.ReturnCode == 0)
            {
                List<TMWService.InvoiceDetail> idList = retObj.ReferenceObjects;
                foreach (TMWService.InvoiceDetail i in idList)
                {
                    Charge ch = (Charge)Enum.Parse(typeof(Charge), i.cht_itemcode);
                    switch (ch)
                    {
                        case Charge.PC:
                            mo.pump_off = i.ivd_quantity.ToString();
                            break;
                    }
                }
            }
            //else
            //{
                
            //    //string msg = string.Join(";", retObj.ErrorMessages.ToArray());
            //    //throw new Exception(msg);
            //}


        }

        private void UpdateTMWOrder(TMWService.Order fuelOrder, string customerSystemOrder)
        {
            fuelOrder.UpdateableProperties = new TMWService.Order.OrderUpdateInfo();
            fuelOrder.ReferenceNumber1 = customerSystemOrder;
            fuelOrder.UpdateableProperties.ReferenceNumber1 = true;
            fuelOrder.ReferenceType1 = mappingType;
            fuelOrder.UpdateableProperties.ReferenceType1 = true;
            InitializeTMWService();
            TMWService.Order.ReturnObject retObj = _tmwWebService.SaveOrder(new List<TMWService.Order> { fuelOrder }, true);
            if (retObj.ReturnCode == 0)
            {
                //return retObj.ReferenceObjects[0];
            }
            else
            {
                string msg = string.Join(";", retObj.ErrorMessages.ToArray());
                throw new Exception(msg);
            }

        }

        private string ExistingOrder(string refType, int DupreOrderNumber)
        {
            InitializeTMWService();
            TMWService.ReferenceNumber.Criteria crit = new TMWService.ReferenceNumber.Criteria();
            crit.ReferenceType = refType;
            crit.OrderHeaderNumber = DupreOrderNumber;
            TMWService.ReferenceNumber.ReturnObject retObj = _tmwWebService.RetrieveReferenceNumber(crit);
            if (retObj.ReturnCode == 0)
            {
                if (retObj.ReferenceObjects.Count > 1)
                {
                    throw new Exception("More than one orders found for McPherson orders found for " + DupreOrderNumber);
                }
                else
                {
                    return retObj.ReferenceObjects[0].Value;
                }
            }
            else if (retObj.ReturnCode == 100 && retObj.ErrorMessages.FirstOrDefault() == "No rows found")
            {
                return null;
            }
            else
            {
                string msg = string.Join(";", retObj.ErrorMessages.ToArray());
                throw new Exception(msg);
            }
        }
        private void InsertToDuprePamsXref(string dupSysOrderNumber)
        {
            using (TMW_DataEntities context = new TMW_DataEntities())
            {
                DuprePamsXref r = context.DuprePamsXrefs.Where(p => p.DupreOrderNo == dupSysOrderNumber).FirstOrDefault();
                if (r == null)
                {
                    r = new DuprePamsXref();
                    r.DupreOrderNo = dupSysOrderNumber;
                    context.DuprePamsXrefs.Add(r);
                    context.SaveChanges();
                }
            }
        }
        private TMWService.Order GetTMWOrder(int orderNumber)
        {
            InitializeTMWService();
            TMWService.Order.ReturnObject retObj = _tmwWebService.GetOrder(orderNumber);
            if (retObj.ReturnCode == 0)
            {
                return retObj.ReferenceObjects[0];
            }
            else
            {
                string msg = string.Join(";", retObj.ErrorMessages.ToArray());
                throw new Exception(msg);
            }
        }
        private string GetRemoteCode(string RecType, string MapType, string TMWCode)
        {
            InitializeTMWService();
            TMWService.InvServicesMapping.Criteria crit = new TMWService.InvServicesMapping.Criteria();
            crit.RecordType = RecType;
            crit.MappingType = MapType;
            crit.TMWKey1 = TMWCode;
            TMWService.InvServicesMapping.ReturnObject retObj = _tmwWebService.RetrieveInvServicesMapping(crit);
            if (retObj.ReturnCode == 0)
            {
                return retObj.ReferenceObjects[0].Key1;
            }
            else
            {
                string msg = string.Join(";", retObj.ErrorMessages.ToArray());
                throw new Exception(RecType + " " + MapType + " " + TMWCode + " " + msg);
            }
        }
        private List<TMWService.InvServicesMapping> GetRemoteCodesList(string RecType, string MapType, string TMWCode)
        {
            InitializeTMWService();
            TMWService.InvServicesMapping.Criteria crit = new TMWService.InvServicesMapping.Criteria();
            crit.RecordType = RecType;
            crit.MappingType = MapType;
            crit.TMWKey1 = TMWCode;
            TMWService.InvServicesMapping.ReturnObject retObj = _tmwWebService.RetrieveInvServicesMapping(crit);
            if (retObj.ReturnCode == 0)
            {
                return retObj.ReferenceObjects;
            }
            else
            {
                string msg = string.Join(";", retObj.ErrorMessages.ToArray());
                throw new Exception(RecType + " " + MapType + " " + TMWCode + " " + msg);
            }
        }
        private string GetSupplierCode(string MapType, string TMWSuppCode, string TMWShipCode)
        {
            List<TMWService.InvServicesMapping> suppList =  GetRemoteCodesList("SUPP", MapType, TMWSuppCode);
            List<TMWService.InvServicesMapping> shipList = GetRemoteCodesList("SHIP", MapType, TMWShipCode);
            List<string> shipCodes = shipList.Select(s => s.Key1).ToList();
            string sCode = suppList.Where(s => shipCodes.Contains(s.Key1)).Select(x => x.Key1).FirstOrDefault();
            return sCode;
        }

        private List<Commodity> GetRemoteCommodities(List<TMWService.Freight> frtList)
        {
            List<Commodity> cmdList = new List<Commodity>();
            foreach (TMWService.Freight fi in frtList)
            {
                foreach (TMWService.FreightByCompartment fc in fi.DeliveryTanks)
                {
                    Commodity cmd = new Commodity();
                    cmd.CommodityCode = GetRemoteCode("CMD", mappingType, fi.CmdCode);
                    cmd.PinCode = fi.PinCode;
                    cmd.TankID = fc.FbcTankNumber;
                    cmd.Volume = fc.FbcVolume;
                    cmd.TankTranslation = GetTankTranslation(fc.FbcConsignee, fc.FbcTankNumber);
                    cmd.BOL = fi.ReferenceNumbers.Where(r => r.ReferenceType == "BL#").Select(r => r.Value).FirstOrDefault();
                    cmd.Supplier = GetSupplierCode(mappingType, fi.Supplier, fi.Shipper);
                    cmdList.Add(cmd);
                }
            }
            return cmdList;
        }

        private string GetTankTranslation(string ConsigneeID, int TankID)
        {
            InitializeTMWService();
            TMWService.CompanyTankDetail.Criteria crit = new TMWService.CompanyTankDetail.Criteria();
            crit.cmp_id = ConsigneeID;
            crit.cmp_tank_id = TankID;
            TMWService.CompanyTankDetail.ReturnObject retObj = _tmwWebService.RetrieveCompanyTankDetail(crit);
            if (retObj.ReturnCode == 0)
            {
                return retObj.ReferenceObjects[0].TankTranslation;
            }
            else
            {
                string msg = string.Join(";", retObj.ErrorMessages.ToArray());
                throw new Exception(msg);
            }
        }

        private string GetXML(List<McpService.SODupre> o)
        {
            StringWriter sw = new StringWriter();
            XmlTextWriter tw = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(o.GetType());
                tw = new XmlTextWriter(sw);
                serializer.Serialize(tw, o);
            }
            catch (Exception ex)
            {
                //Handle Exception Code
            }
            finally
            {
                sw.Close();
                if (tw != null)
                {
                    tw.Close();
                }
            }
            return sw.ToString();
        }

        private string GetXML(McpService.soOutputVO o)
        {
            StringWriter sw = new StringWriter();
            XmlTextWriter tw = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(o.GetType());
                tw = new XmlTextWriter(sw);
                serializer.Serialize(tw, o);
            }
            catch (Exception ex)
            {
                //Handle Exception Code
            }
            finally
            {
                sw.Close();
                if (tw != null)
                {
                    tw.Close();
                }
            }
            return sw.ToString();
        }
        private string GetXML(List<McpBolService.BOL> o)
        {
            StringWriter sw = new StringWriter();
            XmlTextWriter tw = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(o.GetType());
                tw = new XmlTextWriter(sw);
                serializer.Serialize(tw, o);
            }
            catch (Exception ex)
            {
                //Handle Exception Code
            }
            finally
            {
                sw.Close();
                if (tw != null)
                {
                    tw.Close();
                }
            }
            return sw.ToString();
        }
        private string GetXML(List<McpBolService.BOLJDEOutput> o)
        {
            StringWriter sw = new StringWriter();
            XmlTextWriter tw = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(o.GetType());
                tw = new XmlTextWriter(sw);
                serializer.Serialize(tw, o);
            }
            catch (Exception ex)
            {
                //Handle Exception Code
            }
            finally
            {
                sw.Close();
                if (tw != null)
                {
                    tw.Close();
                }
            }
            return sw.ToString();
        }
        #endregion

        #region Logging
        private void LogMessage(List<McpService.SODupre> o, McpService.soOutputVO ou, string process, string msgType, string msgDesc, string localOrder, string remoteOrder, bool isRequestMsg)
        {
            try
            {
                string orderXML = GetXML(o);
                string outputXML = ou != null ? GetXML(ou) : "";
                using (TMW_DataEntities context = new TMW_DataEntities())
                {
                    DupreServiceLog l = new DupreServiceLog();
                    l.AddedBy = "DupreExport";
                    l.AppName = "DupreExport";
                    l.DateAdded = DateTime.Now;
                    l.DateProcessed = DateTime.Now;
                    l.Data = string.Concat(orderXML,outputXML);

                    if (!isRequestMsg)
                    {
                        l.FromService = "McPherson";
                        l.ToService = "Dupre";
                    }
                    else
                    {
                        l.FromService = "Dupre";
                        l.ToService = "McPherson";
                    }
                    l.Process = process;
                    l.MsgType = msgType;
                    l.MsgDesc = msgDesc;
                    l.LocalNumber = localOrder;
                    l.RemoteNumber = remoteOrder;
                    context.DupreServiceLogs.Add(l);
                    context.SaveChanges();
                }

            }
            catch (Exception)
            {

                //throw;
            }
        }
        private void LogMessage(List<McpBolService.BOL> o, List<McpBolService.BOLJDEOutput> ou, string process, string msgType, string msgDesc, string localOrder, string remoteOrder, bool isRequestMsg)
        {
            try
            {
                string orderXML = GetXML(o);
                string outputXML = ou != null ? GetXML(ou) : "";
                using (TMW_DataEntities context = new TMW_DataEntities())
                {
                    DupreServiceLog l = new DupreServiceLog();
                    l.AddedBy = "DupreExport";
                    l.AppName = "DupreExport";
                    l.DateAdded = DateTime.Now;
                    l.DateProcessed = DateTime.Now;
                    l.Data = string.Concat(orderXML, outputXML);

                    if (!isRequestMsg)
                    {
                        l.FromService = "McPherson";
                        l.ToService = "Dupre";
                    }
                    else
                    {
                        l.FromService = "Dupre";
                        l.ToService = "McPherson";
                    }
                    l.Process = process;
                    l.MsgType = msgType;
                    l.MsgDesc = msgDesc;
                    l.LocalNumber = localOrder;
                    l.RemoteNumber = remoteOrder;
                    context.DupreServiceLogs.Add(l);
                    context.SaveChanges();
                }

            }
            catch (Exception)
            {

                //throw;
            }
        }
        private void LogMessage(string process, string msgType, string msgDesc, string localOrder, string remoteOrder, bool isRequestMsg)
        {
            try
            {
                //string orderXML = GetXML(o);
                using (TMW_DataEntities context = new TMW_DataEntities())
                {
                    DupreServiceLog l = new DupreServiceLog();
                    l.AddedBy = "DupreExport";
                    l.AppName = "DupreExport";
                    l.DateAdded = DateTime.Now;
                    l.DateProcessed = DateTime.Now;
                    l.Data = "";

                    if (!isRequestMsg)
                    {
                        l.FromService = "McPherson";
                        l.ToService = "Dupre";
                    }
                    else
                    {
                        l.FromService = "Dupre";
                        l.ToService = "McPherson";
                    }
                    l.Process = process;
                    l.MsgType = msgType;
                    l.MsgDesc = msgDesc;
                    l.LocalNumber = localOrder;
                    l.RemoteNumber = remoteOrder;
                    context.DupreServiceLogs.Add(l);
                    context.SaveChanges();
                }

            }
            catch (Exception)
            {

                //throw;
            }
        }
        #endregion

        #region Service Initializer
        private void InitializeTMWService()
        {
            if (_tmwWebService == null)
            {
                _tmwWebService = new TMWService.APIWCFServicesClient();

            }
        }
        private void CloseTMWService()
        {
            if (_tmwWebService != null)
            {
                _tmwWebService.Close();
                _tmwWebService = null;

            }
        }
        private void InitializeMCPService()
        {
            if (_mcpWebService == null)
            {
                _mcpWebService = new McpService.exportVMISOFromDupreToJDEBPELClient();
            }
        }
        private void CloseMCPService()
        {
            if (_mcpWebService != null)
            {
                _mcpWebService.Close();
                _mcpWebService = null;

            }
        }
        private void InitializeMCPBOLService()
        {
            if (_mcpBolService == null)
            {
                _mcpBolService = new McpBolService.exportBOLfromDupreBizSpeedtoJDEClient();
            }
        }
        private void CloseMCPBOLService()
        {
            if (_mcpBolService != null)
            {
                _mcpBolService.Close();
                _mcpBolService = null;

            }
        }
        #endregion
    }

}
