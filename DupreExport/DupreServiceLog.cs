//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DupreExport
{
    using System;
    using System.Collections.Generic;
    
    public partial class DupreServiceLog
    {
        public int ID { get; set; }
        public System.DateTime DateAdded { get; set; }
        public string Process { get; set; }
        public string MsgType { get; set; }
        public string MsgDesc { get; set; }
        public string AddedBy { get; set; }
        public System.DateTime DateProcessed { get; set; }
        public string Data { get; set; }
        public string AppName { get; set; }
        public string FromService { get; set; }
        public string ToService { get; set; }
        public string LocalNumber { get; set; }
        public string RemoteNumber { get; set; }
        public string Source { get; set; }
    }
}
