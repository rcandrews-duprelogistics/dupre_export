﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DupreExport
{
    class Program
    {
       static void Main(string[] args)
        {
            ExportCriteria ec = new ExportCriteria();

            if (args.Length == 0)
            {
                GetOrderInput(ec, null);
                GetExportTypeInput(ec, null);
            }
            else
            {
                GetOrderInput(ec, args[0]);
                GetExportTypeInput(ec, args[1]);
            }

            Process(ec);
        }
        static void GetOrderInput (ExportCriteria ec, string input)
        {
            if (string.IsNullOrEmpty(input))
            {
            System.Console.WriteLine("Please enter an order number or [ALL] to export all orders.");
            System.Console.WriteLine("Please enter [EXIT] to exit the application.");
            input = Console.ReadLine();
            }

            if (input == "EXIT")
            {
                Environment.Exit(0);
            }

            int ordNumber;

            if (int.TryParse(input, out ordNumber))
            {
                ec.OrderNumber = ordNumber;
            }
            else if (input == "ALL")
            {
                ec.ExportAllOrders = true;
            }
            else
            {
                Console.WriteLine("Invalid order number.",
                                   input == null ? "<null>" : input);
                GetOrderInput(ec, null);
            }


            }
        static void GetExportTypeInput(ExportCriteria ec, string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                System.Console.WriteLine("Please enter OB for ORDER and BOl, O for ORDER only, B for BOL only.");
                System.Console.WriteLine("Please enter [EXIT] to exit the application.");
                input = Console.ReadLine();
            }

            if (input == "EXIT")
            {
                Environment.Exit(0);
            }

            if (input == "O")
            {
                ec.ExportOrder = true;
            }
            else if (input == "B")
            {
                ec.ExportBOL = true;
            }
            else if (input == "OB")
            {
                ec.ExportBOL = true;
                ec.ExportBOL = true;
            }
            else
            {
                Console.WriteLine("Invalid input.",
                                   input == null ? "<null>" : input);
                GetExportTypeInput(ec, null);
            }


        }
        static void Process(ExportCriteria ec)
        {
            MxPhersonService mcp = new MxPhersonService();

            if (ec.ExportAllOrders == false && ec.OrderNumber > 0)
            {
            string msg = mcp.ExportSingleOrder(ec);
            Console.WriteLine(msg);

            }
            else if (ec.ExportAllOrders == true)
            {
                string msg = mcp.ExportAll(ec);
            }
            else
            {
                Console.WriteLine("Export criteria not provided");
            }


        }
    }
}
