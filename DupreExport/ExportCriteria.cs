﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DupreExport
{
    public class ExportCriteria
    {
        public bool ExportAllOrders { get; set; }
        public int OrderNumber { get; set; }
        public bool ExportOrder { get; set; }
        public bool ExportBOL { get; set; }
    }
}
